﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Tedarik_Takip
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'tedarikDataSet1.Müşteri' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            this.müşteriTableAdapter.Fill(this.tedarikDataSet1.Müşteri);
            getir();
            firmagetir();
        }

        private void btnekle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            bag.Open();
            //2. Command nesnesini oluştur.
            SqlCommand komut = new SqlCommand("insert into Müşteri (musteri_Ad,musteri_VergiNo,musteri_Adres,musteri_Tel,musteri_Email,firma_ID) values (@prm1,@prm2,@prm3,@prm4,@prm5,@prm6)", bag);

            komut.Parameters.AddWithValue("@prm1", txtad.Text);
            komut.Parameters.AddWithValue("@prm2", txtvergino.Text);
            komut.Parameters.AddWithValue("@prm3", txtadres.Text);
            komut.Parameters.AddWithValue("@prm4", txttelefon.Text);
            komut.Parameters.AddWithValue("@prm5", txtmail.Text);
            komut.Parameters.AddWithValue("@prm6", cmboxfirma.SelectedValue);
            //3. Commandi çalıştır
            komut.ExecuteNonQuery();
            bag.Close();
            MessageBox.Show("Kayıt İşlemi", "Müşteri Kaydedildi", MessageBoxButtons.OK);
            txtad.Clear();
            txtvergino.Clear();
            txtadres.Clear();
            txttelefon.Clear();
            txtmail.Clear();

            getir();
        }
        void getir()//datagridwiev doldurma kodları
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("SELECT *FROM[Tedarik].[dbo].[Müşteri]", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.Columns["musteri_ID"].Visible = false;
            this.dataGridView1.Columns["firma_ID"].Visible = false;

            dataGridView1.Columns[1].HeaderText = "Ad";
            dataGridView1.Columns[2].HeaderText = "VergiNo";
            dataGridView1.Columns[3].HeaderText = "Adres";
            dataGridView1.Columns[4].HeaderText = "Telefon";
            dataGridView1.Columns[5].HeaderText = "E-Mail";
        }
        public void firmagetir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select * from Firma", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmboxfirma.DataSource = dt;
            cmboxfirma.DisplayMember = "firma_Ad";
            cmboxfirma.ValueMember = "firma_ID";
        }

        private void btnsil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Emin misin?", "Müşteri Silinecek?", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
            {

                foreach (DataGridViewRow drow in dataGridView1.SelectedRows)  //Seçili Satırları Silme
                {
                    int numara = Convert.ToInt32(drow.Cells[0].Value);
                    KayıtSil(numara);
                }
                getir();
                txtad.Clear();
                txtvergino.Clear();
                txtadres.Clear();
                txttelefon.Clear();
                txtmail.Clear();

            }
        }
        void KayıtSil(int numara)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            string sql = "DELETE FROM Müşteri WHERE musteri_ID=@numara";
            SqlCommand komut;
            komut = new SqlCommand(sql, bag);
            komut.Parameters.AddWithValue("@numara", numara);
            bag.Open();
            komut.ExecuteNonQuery();
            bag.Close();
        }

        private void btnguncelle_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult cevap = MessageBox.Show("Müşteri Güncellensin mi?", "Müşteri Güncelle", MessageBoxButtons.YesNo);
                if (cevap == DialogResult.Yes)
                {
                    SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");
                    bag.Open();
                    SqlCommand komut = new SqlCommand("update Müşteri set musteri_Ad='" + txtad.Text + "',musteri_VergiNo='" + txtvergino.Text + "',musteri_Adres='" + txtadres.Text + "',musteri_Tel='" + txttelefon.Text + "',musteri_Email='" + txtmail.Text + "'Where musteri_ID='" + txtId.Text + "'", bag);
                    int rowsaffected = komut.ExecuteNonQuery();
                    if (rowsaffected == 0)
                        MessageBox.Show("Bir hata ile karşılaşıldı");
                    getir();
                    bag.Close();
                    txtad.Clear();
                    txtvergino.Clear();
                    txtadres.Clear();
                    txttelefon.Clear();
                    txtmail.Clear();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Hata:" + ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilialan = dataGridView1.SelectedCells[0].RowIndex; //selectedCells = Seçili alan
            string musteri_Id = dataGridView1.Rows[secilialan].Cells[0].Value.ToString();
            string musteri_Ad = dataGridView1.Rows[secilialan].Cells[1].Value.ToString();
            string musteri_VergiNo = dataGridView1.Rows[secilialan].Cells[2].Value.ToString();
            string musteri_Adres = dataGridView1.Rows[secilialan].Cells[3].Value.ToString();
            string musteri_Tel = dataGridView1.Rows[secilialan].Cells[4].Value.ToString();
            string musteri_Email = dataGridView1.Rows[secilialan].Cells[5].Value.ToString();
            string firma_ID = dataGridView1.Rows[secilialan].Cells[6].Value.ToString();

            txtad.Text = musteri_Ad;
            txtId.Text = musteri_Id;
            txtvergino.Text = musteri_VergiNo;
            txtadres.Text = musteri_Adres;
            txttelefon.Text = musteri_Tel;
            txtmail.Text = musteri_Email;
            cmboxfirma.SelectedValue = firma_ID;

        }


    }
}
