﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tedarik_Takip
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'tedarikDataSet2.Firma' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            this.firmaTableAdapter.Fill(this.tedarikDataSet2.Firma);
            getir();
        }
        void getir()//datagridwiev doldurma kodları
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("SELECT  * FROM [Tedarik].[dbo].[Firma]", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns[1].HeaderText = "Ad";
            dataGridView1.Columns[2].HeaderText = "VergiNo";
            dataGridView1.Columns[3].HeaderText = "Adres";
            dataGridView1.Columns[4].HeaderText = "Telefon";
            dataGridView1.Columns[5].HeaderText = "E-Mail";
            this.dataGridView1.Columns["firma_ID"].Visible = false;

        }

        private void btnguncelle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");
            bag.Open();

            SqlCommand komut = new SqlCommand("update Firma set firma_Ad='" + txtad.Text + "',firma_VergiNo='" + txtvergino.Text + "',firma_Adres='" + txtadres.Text + "',firma_Tel='" + txttelefon.Text + "',firma_Email='" + txtmail.Text +  "'", bag);
            komut.ExecuteNonQuery();
            DialogResult cevap = MessageBox.Show("Firma Güncellensin mi?", "Firma Güncelle", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
                getir();
            bag.Close();
            txtad.Clear();
            txtvergino.Clear();
            txtadres.Clear();
            txttelefon.Clear();
            txtmail.Clear();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int secilialan = dataGridView1.SelectedCells[0].RowIndex; //selectedCells = Seçili alan
            string firma_Ad = dataGridView1.Rows[secilialan].Cells[1].Value.ToString();
            string firma_VergiNo = dataGridView1.Rows[secilialan].Cells[2].Value.ToString();
            string firma_Adres = dataGridView1.Rows[secilialan].Cells[3].Value.ToString();
            string firma_Tel = dataGridView1.Rows[secilialan].Cells[4].Value.ToString();
            string firma_Email = dataGridView1.Rows[secilialan].Cells[5].Value.ToString();

            txtad.Text = firma_Ad;
            txtvergino.Text = firma_VergiNo;
            txtadres.Text = firma_Adres;
            txttelefon.Text = firma_Tel;
            txtmail.Text = firma_Email;

        }
    }
}
