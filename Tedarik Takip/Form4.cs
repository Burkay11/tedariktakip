﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Tedarik_Takip
{
    public partial class Form4 : Form
    {
        private SqlConnection connection;
        public Form4()
        {
            InitializeComponent();
            //database baglanti nesnesini formun yapıcı methodunda oluşturuyoruz bu form kullanıldıgı tum zamanlarda tekrar bir baglanti nesnesi olusturmamiza gerek kalmiyor(herhangi yeni bir connection olusturmadigimiz icin connection pool tasmayacaktir)
            connection = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");
            if (connection.State != ConnectionState.Open)//database baglantimiz acik degilse
                connection.Open();//aciyoruz
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            urunGetir();
            kategorigetir();
        }

        public void kategorigetir()
        {
            SqlDataAdapter da = new SqlDataAdapter("select * from Kategori", connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmboxkategori.DataSource = dt;
            cmboxkategori.DisplayMember = "kategori_Ad";
            cmboxkategori.ValueMember = "kategori_ID";
        }
        void urunGetir()
        {
            SqlDataAdapter da = new SqlDataAdapter("select U.urun_ID as id,U.urun_Ad,K.kategori_Ad from [Tedarik].[dbo].[Ürün] U,[Tedarik].[dbo].[Kategori] K Where U.kategori_ID=K.kategori_ID", connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Ad";
            dataGridView1.Columns[2].HeaderText = "Kategori";

            
        }
        private void btnekle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            bag.Open();
            //2. Command nesnesini oluştur.
            SqlCommand komut = new SqlCommand("insert into Ürün (urun_Ad,kategori_ID) values (@prm1,@prm2)", bag);
            komut.Parameters.AddWithValue("@prm1", txtad.Text);
            komut.Parameters.AddWithValue("@prm2", cmboxkategori.SelectedValue);
            //3. Commandi çalıştır
            komut.ExecuteNonQuery();
            bag.Close();
            MessageBox.Show("Ürün Eklendi", "Ürün", MessageBoxButtons.OK);
            txtad.Clear();
            urunGetir();
        }

        private void btnsil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Emin misin?", "Ürün Silinecek?", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
            {

                short success = 0,unsuccess=0;
                foreach (DataGridViewRow drow in dataGridView1.SelectedRows)  //Seçili Satırları Silme
                {
                    int numara = Convert.ToInt32(drow.Cells[0].Value);
                    if (KayıtSil(numara))
                        success++;
                    else
                        unsuccess++;
                }
                urunGetir();
                txtad.Clear();
               
            }
            bool KayıtSil(int numara)
            {
               
                    string sql = "DELETE FROM Ürün WHERE urun_ID=@numara";
                    SqlCommand komut;
                    komut = new SqlCommand(sql, connection);
                    komut.Parameters.AddWithValue("@numara", numara);
                   int rowCount= komut.ExecuteNonQuery();

                if (rowCount > 0)
                    return true;
                else
                    return false;
                
            }
        }

      
    }
}
