﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Tedarik_Takip
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'tedarikDataSet4.Kategori' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            this.kategoriTableAdapter.Fill(this.tedarikDataSet4.Kategori);
            getir();

        }

        private void btnekle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            bag.Open();
            //2. Command nesnesini oluştur.
            SqlCommand komut = new SqlCommand("insert into Kategori (kategori_Ad) values (@prm1)", bag);

            komut.Parameters.AddWithValue("@prm1", txtad.Text);


            //3. Commandi çalıştır
            komut.ExecuteNonQuery();
            bag.Close();
            MessageBox.Show("Kayıt İşlemi", "Kategori Eklendi", MessageBoxButtons.OK);
            txtad.Clear();
            getir();
        }
        void getir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select Kategori.kategori_ID,Kategori.kategori_Ad from Kategori", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.Columns["kategori_ID"].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Ad";

        }

    

        private void btnsil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Emin misin?", "Kategori Silinecek?", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
            {

                foreach (DataGridViewRow drow in dataGridView1.SelectedRows)  //Seçili Satırları Silme
                {
                    int numara = Convert.ToInt32(drow.Cells[0].Value);
                    KayıtSil(numara);
                }
                getir();
                txtad.Clear();
            }
            void KayıtSil(int numara)
            {
                SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
                string sql = "DELETE FROM Kategori WHERE kategori_ID=@numara";
                SqlCommand komut;
                komut = new SqlCommand(sql, bag);
                komut.Parameters.AddWithValue("@numara", numara);
                bag.Open();
                komut.ExecuteNonQuery();
                bag.Close();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilialan = dataGridView1.SelectedCells[0].RowIndex; //selectedCells = Seçili alan
            string kategori_Ad = dataGridView1.Rows[secilialan].Cells[1].Value.ToString();



            txtad.Text = kategori_Ad;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
