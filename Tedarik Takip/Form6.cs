﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Tedarik_Takip
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();

        }

        private void Form6_Load(object sender, EventArgs e)
        {
            getir();
            urungetir();
        }

        public void urungetir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select * from Ürün", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmboxad.DataSource = dt;
            cmboxad.DisplayMember = "urun_Ad";
            cmboxad.ValueMember = "urun_ID";
        }

        void getir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select Stok.stok_ID,Stok.stok_Ad,Stok.urun_ID,Ürün.urun_Ad from Stok, Ürün where Ürün.urun_ID=Stok.urun_ID", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
           this.dataGridView1.Columns["urun_ID"].Visible = false;
            this.dataGridView1.Columns["stok_ID"].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Adet";
            dataGridView1.Columns[3].HeaderText = "Ürün Ad";
          
        }

        private void btnekle_Click_1(object sender, EventArgs e)
        {

            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            bag.Open();
            //2. Command nesnesini oluştur.
            SqlCommand komut = new SqlCommand("insert into Stok (stok_Ad,urun_ID) values (@prm1,@prm2)", bag);
            komut.Parameters.AddWithValue("@prm1", txtadet.Text);
            komut.Parameters.AddWithValue("@prm2", cmboxad.SelectedValue);
            //3. Commandi çalıştır
            komut.ExecuteNonQuery();
            bag.Close();
            MessageBox.Show("Kayıt İşlemi", "Stok Kaydedildi", MessageBoxButtons.OK);
            txtadet.Clear();
            getir();
        }

        private void btnsil_Click_1(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Emin misin?", "Stok Silinecek?!", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
            {

                foreach (DataGridViewRow drow in dataGridView1.SelectedRows)  //Seçili Satırları Silme
                {
                    int numara = Convert.ToInt32(drow.Cells[0].Value);
                    KayıtSil(numara);
                }
                getir();
                txtadet.Clear();
            }
            void KayıtSil(int numara)
            {
                SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
                string sql = "DELETE FROM Stok WHERE stok_ID=@numara";
                SqlCommand komut;
                komut = new SqlCommand(sql, bag);
                komut.Parameters.AddWithValue("@numara", numara);
                bag.Open();
                komut.ExecuteNonQuery();
                bag.Close();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilialan = dataGridView1.SelectedCells[0].RowIndex; //selectedCells = Seçili alan
            string stok_Ad = dataGridView1.Rows[secilialan].Cells[1].Value.ToString();
            string urun_ID = dataGridView1.Rows[secilialan].Cells[2].Value.ToString();
            txtadet.Text = stok_Ad;           
            cmboxad.SelectedValue = urun_ID;
        }

        private void btnguncelle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");
            bag.Open();

            SqlCommand komut = new SqlCommand("update Stok set stok_Ad='" + txtadet.Text + "'Where urun_ID='" + cmboxad.SelectedValue + "'", bag);
            komut.ExecuteNonQuery();
            DialogResult cevap = MessageBox.Show("Stok Güncellensin mi?", "Stok Güncelle", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
                getir();
            bag.Close();
            txtadet.Clear();
        }
    }
}

