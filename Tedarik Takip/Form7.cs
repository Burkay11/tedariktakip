﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace Tedarik_Takip
{
    public partial class Form7 : Form
    {



        public Form7()
        {
            InitializeComponent();

        }

        void KayıtSil(int numara)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            string sql = "DELETE FROM Cari WHERE cari_ID=@numara";
            SqlCommand komut;
            komut = new SqlCommand(sql, bag);
            komut.Parameters.AddWithValue("@numara", numara);
            bag.Open();
            int rowsaffected=komut.ExecuteNonQuery();
            bag.Close();
        }


        private void Form7_Load(object sender, EventArgs e)
        {
            getir();
            urungetir();
            musterigetir();            
        }


        void urungetir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select * from Ürün", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmboxurunadı.DataSource = dt;
            cmboxurunadı.DisplayMember = "urun_Ad";
            cmboxurunadı.ValueMember = "urun_ID";
        }


        void musterigetir()
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select * from Müşteri", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmboxmusteriad.DataSource = dt;
            cmboxmusteriad.DisplayMember = "musteri_Ad";
            cmboxmusteriad.ValueMember = "musteri_ID";

        }
        void getir()//datagridwiev doldurma kodları
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            SqlDataAdapter da = new SqlDataAdapter("select Cari.cari_SatılanAdet,Cari.cari_ID,Cari.cari_Satış,Cari.urun_ID,Ürün.urun_Ad,Cari.musteri_ID,Müşteri.musteri_Ad from Cari inner join Ürün on Cari.urun_ID=Ürün.urun_ID inner join Müşteri on Cari.musteri_ID=Müşteri.musteri_ID", bag);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.Columns["cari_ID"].Visible = false;
            this.dataGridView1.Columns["musteri_ID"].Visible = false;
            this.dataGridView1.Columns["urun_ID"].Visible = false;
        }



        private void btnekle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");//1.
            bag.Open();
            //2. Command nesnesini oluştur.
            SqlCommand komut = new SqlCommand("insert into Cari (cari_Satış,cari_SatılanAdet,urun_ID,musteri_ID) values (@prm1,@prm2,@prm3,@prm4)", bag);

            komut.Parameters.AddWithValue("@prm1", txtsatis.Text);
            komut.Parameters.AddWithValue("@prm2", txtadet.Text);
            komut.Parameters.AddWithValue("@prm3", cmboxurunadı.SelectedValue);
            komut.Parameters.AddWithValue("@prm4", cmboxmusteriad.SelectedValue);
       
            //3. Commandi çalıştır
            komut.ExecuteNonQuery();
            bag.Close();
            MessageBox.Show("Kayıt İşlemi", "Müşteri Kaydedildi", MessageBoxButtons.OK);
            txtadet.Clear();
            txtsatis.Clear();
        

            getir();

        }

        private void btnsil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Emin misin?", "Müşteri Silinecek?", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
            {

                foreach (DataGridViewRow drow in dataGridView1.SelectedRows)  //Seçili Satırları Silme
                {
                    int numara = Convert.ToInt32(drow.Cells[1].Value);
                    KayıtSil(numara);
                }
                getir();
               
           

            }
        }
    
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          

        }

        private void btnguncelle_Click(object sender, EventArgs e)
        {
            SqlConnection bag = new SqlConnection("server=DESKTOP-TCOKTK9;initial catalog=Tedarik;integrated security=true");
            bag.Open();

            SqlCommand komut = new SqlCommand("update Cari set cari_Satış='" + txtsatis.Text + "',cari_SatılanAdet='" + txtadet.Text + "',urun_ID='" + cmboxurunadı.SelectedValue + "'Where musteri_ID='" + cmboxmusteriad.SelectedValue + "'", bag);
            komut.ExecuteNonQuery();
            DialogResult cevap = MessageBox.Show("Müşteri Güncellensin mi?", "Müşteri Güncelle", MessageBoxButtons.YesNo); if (cevap == DialogResult.Yes)
                getir();
            bag.Close();
            txtadet.Clear();
            txtsatis.Clear();

        }


    }
}
